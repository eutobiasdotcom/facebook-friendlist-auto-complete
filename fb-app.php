<?php

class FBApp {

	public $facebook;
	public $user;
	public $friendlist;
	
	public function init()
	{
		$this->facebook = new Facebook(array(
			'appId'  => FBAppConfig::$APP_ID,
			'secret' => FBAppConfig::$APP_SECRET,
			'cookie' => true
		));
	}

	public function authorize()
	{
		$this->user = $this->facebook->getUser();

		if (!$this->user)
		{
			header('Location: '. $this->facebook->getLoginUrl());
			die;
		}
	}

	// TODO - Implementar outros métodos de saída
	public function getFriendList( $output = 'json' )
	{
		//me/friends?fields=name,picture
		try 
		{
			$this->friendlist = $this->facebook->api('/me/friends?fields=name,picture');
			
			if ($output == 'json')
			{	
				return json_encode($this->friendlist);
			}
		}
		catch (FacebookApiException $e)
		{
			 die($e);
		}
	}
	
	function __construct()
	{
		$this->init();
		$this->authorize();
	}

}

?>