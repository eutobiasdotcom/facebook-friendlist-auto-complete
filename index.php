<?php

require_once("sdk/src/facebook.php");
require_once("fb-config.php");
require_once("fb-app.php");

$FBApp = new FBApp();
$FBApp->getFriendList();
?>

<!doctype html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Facebook App Example</title>
		<style>
			span.highlight { background: #ff0; font-weight: bold; }

			div#auto_complete {width:300px;border: 1px solid #000;background: #f0f0f0;display:none;}
			div#auto_complete ul li {list-style: none;}

		</style>
	</head>
	<body>
		<header>
			<h1>Facebook App Example</h1>
			<p class="highlight">This is a Facebook app example that show you all your contacts in a auto complete list.</p>
		</header>
		<section>
			<input type="text" id="auto_complete_friends">
			
			<div id="auto_complete">
				<ul>
					<li><img src="" alt=""><span></span></li>
					<li><img src="" alt=""><span></span></li>
					<li><img src="" alt=""><span></span></li>
					<li><img src="" alt=""><span></span></li>
					<li><img src="" alt=""><span></span></li>
				</ul>
			</div>

		</section>

		<script src="js/jquery-1.10.2.min.js"></script>
		<script>
			String.prototype.iIndexOf = function (s, b) {
				return this.toLowerCase().indexOf(s.toLowerCase(), b);
			}
			var friendlist = <?php echo $FBApp->getFriendList('json'); ?>;
			
			function populateAutoComplete(itens, input)
			{	
				var box = $('<ul></ul>');
				var item = '<li id="##ID##"><img src="##IMG##"><span>##NAME##</span></li>'

				for (var i in itens)
				{
					var tmp;
						tmp=item.replace('##ID##', itens[i].id);
						tmp=tmp.replace('##IMG##', itens[i].picture.data.url);
						tmp=tmp.replace('##NAME##', highlightText(itens[i].name, input));

					box.append(tmp);	
				}

				$('body>section>div#auto_complete').html(box);
			}

			function highlightText(string, highlight)
			{
				return string.replace(highlight, '<span class="highlight">'+highlight+'</span>');
			}

			$(function() {
				$('#auto_complete_friends').on("keyup", function(){
					var input = $(this).val();
					var output = [];

					if (input.length >= 3)
					{
						$('div#auto_complete').show();

						for (var i in friendlist.data)
						{
							if (friendlist.data[i].name.iIndexOf(input) > -1)
								output.push(friendlist.data[i])
						}
					}
					else
					{
						$('div#auto_complete').hide();
					}

					populateAutoComplete(output, input);
				});
			});
		</script>
	</body>
</html>